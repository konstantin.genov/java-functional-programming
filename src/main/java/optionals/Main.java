package optionals;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        // declarative
        Object value = Optional.ofNullable(null)
                .orElse("default value");
        System.out.println(value);

        // imperative
        if (value != null) {
            System.out.println(value);
        } else {
            System.out.println("default value imperative");
        }

        // if a value is present execute consumer
        Optional.ofNullable(null)
                .ifPresent(System.out::println);

        // if else example, takes in consumer then a runnable
        Optional.ofNullable(null)
                .ifPresentOrElse(
                        email -> System.out.println("Sending email to: " + email),
                        () -> System.out.println("No email provided")
                );
    }
}
