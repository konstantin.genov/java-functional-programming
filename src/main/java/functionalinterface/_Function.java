package functionalinterface;

import java.util.function.BiFunction;
import java.util.function.Function;

public class _Function {
    public static void main(String[] args) {
        // Function takes 1 argument and produces 1 result
        System.out.println("Function Example");
        int increment = incrementByOne(1);
        System.out.println(increment);

        int increment2 = incrementByOneFunction.apply(1);
        System.out.println(increment2);

        int multiply = multiplyByTenFunction.apply(increment2);
        System.out.println(multiply);

        // Chaining functions to reach desired state:
        Function<Integer, Integer> incrementByOneAndMultiplyBy10 =
                incrementByOneFunction.andThen(multiplyByTenFunction);
        System.out.println(incrementByOneAndMultiplyBy10.apply(5));

        System.out.println(incrementByOneFunction.andThen(multiplyByTenFunction).apply(10));

        // BiFunction takes 2 arguments and produces 1 result:
        System.out.println("BiFunction Example");

        System.out.println(incrementByOneAndMultiplyBiFunction.apply(3, 69));


    }

    // Data types: <input, ouput>
    // with interface
    static Function<Integer, Integer> incrementByOneFunction =
            number -> number + 1;

    static Function<Integer, Integer> multiplyByTenFunction =
            number -> number * 10;

    // without interface
    static int incrementByOne(int number) {
        return number + 1;
    }

    // with interface
    static BiFunction<Integer, Integer, Integer> incrementByOneAndMultiplyBiFunction =
            (numberToIncrementByOne, numberToMultiplyBy)
                    -> (numberToIncrementByOne + 1) * numberToMultiplyBy;

    // without interface
    static int incrementByOneAndMultiply(int number, int numToMultiplyBy) {
        return (number + 1) * numToMultiplyBy;
    }
}
