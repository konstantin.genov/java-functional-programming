package functionalinterface;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

// represents an operation that accepts a single input and returns no result.
public class _Consumer {

    public static void main(String[] args) {
        Customer customer = new Customer("Boiko", "420503263");
        // normal java func
        greetCustomer(customer);

        // consumer functional interface
        greetCustomerConsumer.accept(customer);

        // biconsumer
        greetCustomerConsumerv2.accept(customer, false);
    }

    // the old way
    static void greetCustomer(Customer customer) {
        System.out.println("Hello " + customer.customerName + ", thanks for registering phone number: " + customer.customerPhoneNumber);
    }

    // achieve the same but by using the consumer functional interface
    static Consumer<Customer> greetCustomerConsumer = customer ->
            System.out.println("Hello " + customer.customerName
                    + ", thanks for registering phone number: "
                    + customer.customerPhoneNumber);

    static BiConsumer<Customer, Boolean> greetCustomerConsumerv2 = (customer, showPhoneNumber) ->
            System.out.println("Hello " + customer.customerName
                    + ", thanks for registering phone number: "
                    + (showPhoneNumber ? customer.customerPhoneNumber : "GDPR"));

    static class Customer {
        private final String customerName;
        private final String customerPhoneNumber;

        public Customer(String customerName, String customerPhoneNumber) {
            this.customerName = customerName;
            this.customerPhoneNumber = customerPhoneNumber;
        }
    }
}
