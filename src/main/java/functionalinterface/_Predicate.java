package functionalinterface;

import java.util.function.Predicate;

// predicate represents a predicate (boolean-valued function) of one argument
public class _Predicate {

    public static void main(String[] args) {
        System.out.println("Without predicate");
        System.out.println(isPhoneNumberValid("+35989432323"));
        System.out.println(isPhoneNumberValid("0894"));

        System.out.println("With predicate");
        System.out.println(isPhoneNumberValidPredicate.test("+35989432323"));
        System.out.println(isPhoneNumberValidPredicate.test("+0894"));

        System.out.println("Is phone number valid and contains number 3: " + isPhoneNumberValidPredicate.and(containsNumber3).test("+35989432323"));


    }

    // normal java function returning a bool
    static boolean isPhoneNumberValid(String phoneNumber) {
        // simulate validation
        return phoneNumber.startsWith("+359") && phoneNumber.length() > 5;
    }

    static Predicate<String> isPhoneNumberValidPredicate = phoneNumber ->
            phoneNumber.startsWith("+359") && phoneNumber.length() > 5;

    static Predicate<String> containsNumber3 = phoneNumber -> phoneNumber.contains("3");
}
