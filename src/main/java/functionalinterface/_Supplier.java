package functionalinterface;

import java.util.function.Supplier;

// represents a supplier of results
public class _Supplier {

    public static void main(String[] args) {
        System.out.println(getDBConnectionURL());
        System.out.println(getDBConnectionSupplier.get());
    }

    static String getDBConnectionURL () {
        return "jdbc://localhost:5432/users";
    }

    static Supplier<String> getDBConnectionSupplier = () -> "jdbc://localhost:5432/users";

}
