package imperative;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static imperative.Main.Gender.FEMALE;
import static imperative.Main.Gender.MALE;


public class Main {
    public static void main(String[] args) {
        List<Person> people = List.of(
                new Person("Konstantin", MALE),
                new Person("Maria", FEMALE),
                new Person("Alex", MALE),
                new Person("Alice", FEMALE),
                new Person("Siri", FEMALE)
        );

        // With an imperative approach, a developer writes code that specifies the steps that the computer must take
        // to accomplish the goal. This is sometimes referred to as algorithmic programming.
        // Imperative approach:
        List<Person> females = new ArrayList<>();

        for (Person person : people) {
            if (FEMALE.equals(person.gender)){
                females.add(person);
            }
        }
        System.out.println("// Imperative approach");
        for (Person female : females) {
            System.out.println(female);
        }

        // The functional programming paradigm was explicitly created to support a pure functional approach to problem-solving.
        // Functional programming is a form of declarative programming.
        // Declarative approach:
        System.out.println("// Declarative approach");
        List<Person> femalesDec = people.stream()
                .filter(person -> FEMALE.equals(person.gender)).toList();
        femalesDec.forEach(System.out::println);
    }

    static class Person{
        private final String name;
        private final Gender gender;

        public Person(String name, Gender gender) {
            this.name = name;
            this.gender = gender;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", gender=" + gender +
                    '}';
        }
    }

    enum Gender {
        MALE, FEMALE
    }
}
